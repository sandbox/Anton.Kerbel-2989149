A config entity manager. It will allow you to create, edit, delete and translate config entities. It will be helpful for developers who has to create a lot of custom website configs.

It has two tabs:
1. "Aperto Config" tab. A tab for content managers. Content managers can work with config entity values and value translations. Each language in drupal will get a special subtab here.
2. "Variables" tab. A tab for admins. Admins can create, edit and delete config entities here. Also there is an action link to drupal translation page to translate titles and descriptions of config entities.

**Features**
1. Config entity values separated from config entity management. You can create one user role only for working with values and another for working with config entity structure.
2. The module works with regular drupal config entites, so they can be exported or imported as any other custom config entities.
3. Full translatable. Titles, descriptions and values can be translated. You can have different values for different languages even if it is a checkbox for example.
4. "Variables tab" is a draggable list builder. It means you can change a row and inheritance of your elements only by moving a mouse.

**Available form elements**
1. Textfield
2. Textarea
3. Textformat
4. Checkbox
5. Select
6. Radio buttons
7. Entity autocomplete
8. Fieldset(details)
9. Vertical tabs

**Known issues**
1. Format value for "text format" element is not translatable. Format can be chosen only on default language page and one for all languages.

**Future plans**

1. More visual settings. Sizes of textfields and etc.
2. Improving of "options" form in elements with options(radios, select).
3. Autotests.

Example how to use config entities in your custom code:
```php
$config = \Drupal::entityTypeManager()->getStorage('aperto_config_entity');
$entity = $config->load('config_entity_machine_name');
$text = $entity->getValue();
```