<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:48
 */

namespace Drupal\aperto_config_entity;


interface ApertoConfigElementInterface
{
  /**
   * Return Form API form element
   *
   * @return array
   */
  public function getFormElement(): array;

  /**
   * Set value for ApertoConfigEntity regarding form element
   *
   * @param $value
   * @return mixed
   */
  public function setValue($value);

  /**
   * Get the value of the variable
   *
   * @return mixed
   */
  public function getValue();

  /**
   * Set value as empty after creation of the variable. Some field types cannot be NULL and they rewrite this method to their own.
   */
  public function setEmpty();

  /**
   * Get drupal form element type. Is used as options in add form.
   *
   * @return string
   */
  public static function getType(): string;

  /**
   * Get visible label for element type
   *
   * @return string
   */
  public static function getLabel(): string;

  /**
   * Return array of form elements for baseform
   *
   * @return array
   */
  public function settingsForm(): array;
}