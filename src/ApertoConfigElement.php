<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:50
 */

namespace Drupal\aperto_config_entity;

use \Drupal\aperto_config_entity\Entity\ApertoConfigEntity;
use \Drupal\Core\Form\FormStateInterface;

abstract class ApertoConfigElement implements ApertoConfigElementInterface
{
  /**
   * @var ApertoConfigEntity
   */
  protected $apertoConfigEntity;

  /**
   * ApertoConfigElement constructor.
   *
   * @param ApertoConfigEntity $apertoConfigEntity
   */
  public function __construct(ApertoConfigEntity $apertoConfigEntity)
  {
    $this->apertoConfigEntity = $apertoConfigEntity;
  }

  /**
   * Get an instance of Aperto Config Element
   *
   * @param ApertoConfigEntity $config
   * @return ApertoConfigElement
   */
  public static function getInstance(ApertoConfigEntity $config): self
  {
    $className = ApertoConfigElement::getElementClassName($config->type);
    return new $className($config);
  }

  /**
   * Get element class name regarding config field type
   *
   * @param $type
   * @return string
   */
  public static function getElementClassName($type): string
  {
    if (empty($type)) {
      $type = 'textfield';
    }
    return '\Drupal\aperto_config_entity\Element\ApertoConfig' . ucfirst($type);
  }

  /**
   * Set value.
   *
   * @param $value
   *
   * @return void
   */
  public function setValue($value)
  {
    $this->apertoConfigEntity->value = $value;
  }

  /**
   * Get value
   *
   * @return mixed
   */
  public function getValue()
  {
    return $this->apertoConfigEntity->value;
  }

  /**
   * Set value as empty after creation of the variable. Some field types cannot be NULL and they rewrite this method to their own.
   */
  public function setEmpty()
  {
    $this->apertoConfigEntity->value = NULL;
  }

  /**
   * Prepare settings array from add/edit form values for saving
   *
   * @param array $values
   * @return array
   */
  public function prepareSettings(array $values): array
  {
    $result = [];
    $keys = array_keys($this->settingsForm());

    foreach ($keys as $key) {
      if (isset($values[$key])) {
        $result[$key] = $values[$key];
      }
    }
    return $result;
  }

  /**
   * Get the list of all element types
   *
   * @return array
   */
  public static function getAllTypes(): array
  {
    $result = [];
    $path = __DIR__ . '/Element/';
    $files = scandir($path);

    foreach ($files as $file) {
      if (preg_match('/^ApertoConfig\w+\.php$/', $file)) {
        $classname = '\\Drupal\\aperto_config_entity\\Element\\' . str_replace('.php', '',$file);
        $result[$classname::getType()] = $classname::getLabel();
      }
    }

    asort($result);

    return $result;
  }

  /**
   * Is element a container (details element, etc)
   *
   * @return bool
   */
  public function isContainer(): bool
  {
    if (isset($this->isContainer) && $this->isContainer) {
      return true;
    }

    return false;
  }

  /**
   * Return TRUE if the element is a "vertical tabs" element
   *
   * @return bool
   */
  public function isTabs(): bool
  {
    return $this->apertoConfigEntity->type == 'tabs';
  }

}