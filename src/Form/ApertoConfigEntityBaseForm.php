<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 06.06.18
 * Time: 16:29
 */

namespace Drupal\aperto_config_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\aperto_config_entity\ApertoConfigElement;

class ApertoConfigEntityBaseForm extends EntityForm
{
  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Construct
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   */
  public function __construct(EntityStorageInterface $entity_storage)
  {
    $this->entityStorage = $entity_storage;
  }

  /**
   * Factory method
   */
  public static function create(ContainerInterface $container)
  {
    $form = new static($container->get('entity_type.manager')->getStorage('aperto_config_entity'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity add/edit form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An associative array containing the robot add/edit form.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // Get anything we need from the base class.
    $form = parent::buildForm($form, $form_state);

    $config = $this->entity;

    // Build the form.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)|(^custom$)',
        'error' => 'The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores. Additionally, it can not be the reserved word "custom".',
      ],
      '#disabled' => !$config->isNew(),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => ApertoConfigElement::getAllTypes(),
      '#default_value' => $config->type ?? '',
      '#disabled' => !$config->isNew(),
      '#empty_value' => '',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::settingsCallback',
        'event' => 'change',
        'wrapper' => 'settings',
      ],
    ];

    $form['parent'] = $config->getParentSelectElement();
    $form['parent']['#title'] = 'Parent container';

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $config->description,
    ];

    $form['required'] = [
      '#type' => 'checkbox',
      '#title' => 'Required',
      '#default_value' => $config->required,
      '#disabled' => $config->getElement()->isContainer(),
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => 'Additional Settings',
      '#open' => TRUE,
      '#attributes' => [
        'id' => ['settings'],
      ],
    ];

    $form['settings'] = array_merge($form['settings'], $config->getElement()->settingsForm());

    // Return the form.
    return $form;
  }

  /**
   * Ajax callback
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function settingsCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['settings'];
  }

  /**
   * Checks for an existing config.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new config entity query.
    $query = $this->entityStorage->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   *
   * To set the submit button text, we need to override actions().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An array of supported actions for the current entity form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Get the basic actins from the base class.
    $actions = parent::actions($form, $form_state);

    // Change the submit button text.
    $actions['submit']['#value'] = $this->t('Save');

    // Return the result.
    return $actions;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::validate().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function validate(array $form, FormStateInterface $form_state) {
    parent::validate($form, $form_state);

    // Add code here to validate your config entity's form elements.
    // Nothing to do here.
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * Saves the entity. This is called after submit() has built the entity from
   * the form values. Do not override submit() as save() is the preferred
   * method for entity form controllers.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {

    // EntityForm provides us with the entity we're working on.
    $config = $this->getEntity();

    $config->settings = $config->getElement()->prepareSettings($form_state->getValues());

    // Some field types cannot be NULL and they need to set something like empty string instead of default NULL
    if ($config->isNew() || empty($config->value)) {
      $config->getElement()->setEmpty();
    }

    // Drupal already populated the form values in the entity object. Each
    // form field was saved as a public variable in the entity class. PHP
    // allows Drupal to do this even if the method is not defined ahead of
    // time.
    $status = $config->save();

    // Grab the URL of the new entity. We'll use it in the message.
    $url = $config->toUrl();

    // Create an edit link.
    $edit_link = Link::fromTextAndUrl($this->t('Edit'), $url)->toString();

    if ($status == SAVED_UPDATED) {
      // If we edited an existing entity...
      $this->messenger()->addMessage($this->t('Variable %label has been updated.', ['%label' => $config->label()]));
      $this->logger('contact')->notice('Variable %label has been updated.', ['%label' => $config->label(), 'link' => $edit_link]);
    }
    else {
      // If we created a new entity...
      $this->messenger()->addMessage($this->t('Variable %label has been added.', ['%label' => $config->label()]));
      $this->logger('contact')->notice('Variable %label has been added.', ['%label' => $config->label(), 'link' => $edit_link]);
    }

    // Redirect the user back to the listing route after the save operation.
    $form_state->setRedirect('aperto_config_entity');
  }

}