<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 14:39
 */

namespace Drupal\aperto_config_entity\Form;

use \Drupal\Core\Form\{FormBase, FormStateInterface};
use \Drupal\Core\Language\LanguageManagerInterface;
use \Drupal\Core\Url;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Symfony\Component\DependencyInjection\ContainerInterface;

class ApertoConfigEntityListForm extends FormBase
{

  /**
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'aperto_config_entity_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $languageManager) {
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // Language setup
    $this->languageSetup();

    // Build the form
    $configs = \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->loadMultiple();
    uasort($configs, array('\Drupal\Core\Config\Entity\ConfigEntityBase', 'sort'));

    $build = $this->buildBranch($configs, '');

    if (empty($build)) {
      $build['#markup'] = $this->t('Sorry, There are no items!') . '<br>';
    }

    $build['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this
        ->t('Save'),
    );

    return $build;
  }

  /**
   * Build an elements branch in Form API array
   *
   * @param array $configs
   * @param string $parent
   * @return array
   */
  private function buildBranch(array $configs, string $parent = ''): array
  {
    $result = [];

    foreach ($configs as $config) {
      if ($parent == $config->parent) {
        $result[$config->id] = $config->getValueFormElement();
        $result[$config->id]['#title'] = $config->label;
        $result[$config->id]['#description'] = $config->description;

        if ($config->getElement()->isContainer()) {
          if ($config->getElement()->isTabs()) {
            $result = array_merge($result, $this->buildBranch($configs, $config->id));
          } else {
            $result[$config->id] = array_merge($result[$config->id], $this->buildBranch($configs, $config->id));
          }
        }
      }
    }

    return $result;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state){

    $langcode = $this->getRequest()->attributes->get('langcode');
    $this->languageSetup();
    if (!empty($langcode)) {
      $this->saveTranslatedValues($form_state->getValues(), $langcode);
    } else {
      $this->saveValues($form_state->getValues());
    }
  }

  /**
   * Save original values without translations
   *
   * @param array $values
   * @return $this
   */
  protected function saveValues(array $values) {

    $configs = \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->loadMultiple();
    $keys = array_keys($configs);
    $status = null;

    foreach ($keys as $key) {
      if (array_key_exists($key, $values)) {
        $configs[$key]->getElement()->setValue($values[$key]);
        $status = $configs[$key]->save();
      }
    }

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t('Aperto Config has been updated.'));
      $this->logger('config')->notice('Aperto Config has been updated.');
    }

    return $this;
  }

  /**
   * Save translated values
   *
   * @param array $values
   * @param string $langcode
   * @return $this
   */
  protected function saveTranslatedValues(array $values, string $langcode)
  {
    $originals = \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->loadMultiple();
    $keys = array_keys($originals);

    foreach ($keys as $key) {
      if (array_key_exists($key, $values)) {
        $config = $this->languageManager->getLanguageConfigOverride($langcode, 'aperto_config_entity.settings.' . $key);

        // @todo "text format" element has value as array.
        // But translation object doesn`t know anything about element and can't inherit its logic.
        $config->set('value', !is_array($values[$key]) ? $values[$key] : $values[$key]['value']);
        $config->save();
      }
    }

    // Class LanguageConfigOverride doesn`t return save status, so we have just to write message after any result
    $this->messenger()->addMessage($this->t('Aperto Config for [' . $langcode . '] has been updated.'));
    $this->logger('config')->notice('Aperto Config for [' . $langcode . '] has been updated.');

    return $this;
  }

  /**
   * Language set up. It allows us to depend on langcode in route and don`t depend on user language
   *
   * @return $this|RedirectResponse
   */
  protected function languageSetup()
  {
    $langcode = $this->getRequest()->attributes->get('langcode');

    if (!empty($langcode)) {
      // If somebody tries to work with default language as with translation, redirect him to original data page
      if ($langcode == $this->languageManager->getDefaultLanguage()->getId()) {
        $this->redirectToOriginal();
      }
      // Get the language
      $language = $this->languageManager->getLanguage($langcode);
    } else {
      // We also have to set up the language for original data page,
      // otherwise german users will always see german translations and won't be able to see original data at all.
      $language = $this->languageManager->getDefaultLanguage();
    }
    // Check the language exist
    if (!$language) {
      throw new NotFoundHttpException('Language [' . $langcode . '] not found');
    }
    // Set the language up
    $this->languageManager->setConfigOverrideLanguage($language);

    return $this;
  }

  /**
   * Redirect to original data page. Used when somebody tries to work with default language as with translation.
   */
  protected function redirectToOriginal()
  {
    $response = new RedirectResponse(Url::fromRoute('aperto_config_entity.list')->toString());
    $response->send();
  }
}