<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 06.06.18
 * Time: 15:25
 */

namespace Drupal\aperto_config_entity\Entity;

use \Drupal\aperto_config_entity\ApertoConfigElement;
use \Drupal\Core\Config\Entity\ConfigEntityBase;
use \Drupal\aperto_config_entity\ApertoConfigElementInterface;

/**
 * Defines the Aperto Config entity.
 *
 * $entity = \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->load('test1');
 *
 *
 * @ConfigEntityType(
 *   id = "aperto_config_entity",
 *   label = @Translation("Aperto Config"),
 *   handlers = {
 *     "list_builder" = "Drupal\aperto_config_entity\ApertoConfigEntityDragabbleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\aperto_config_entity\Form\ApertoConfigEntityAddForm",
 *       "edit" = "Drupal\aperto_config_entity\Form\ApertoConfigEntityBaseForm",
 *       "delete" = "Drupal\aperto_config_entity\Form\ApertoConfigEntityDeleteForm",
 *     }
 *   },
 *   translatable = TRUE,
 *   config_prefix = "settings",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "type" = "type",
 *     "description" = "description",
 *     "value" = "value",
 *     "weight" = "weight",
 *     "parent" = "parent",
 *     "settings" = "settings"
 *   },
 *   links = {
 *     "edit-form" = "/admin/aperto_config_entity/edit/{aperto_config_entity}",
 *     "delete-form" = "/admin/aperto_config_entity/delete/{aperto_config_entity}",
 *   }
 * )
 */
class ApertoConfigEntity extends ConfigEntityBase
{
  /**
   * @var string
   */
  public $id;

  /**
   * @var string
   */
  public $type;

  /**
   * @var string
   */
  public $label;

  /**
   * @var string
   */
  public $description;

  /**
   * @var mixed
   */
  public $value;

  /**
   * @var integer
   */
  public $weight;

  /**
   * @var string
   */
  public $parent;

  /**
   * @var boolean
   */
  public $required;

  /**
   * @var array
   */
  public $settings;

  /**
   * ApertoConfigEntity constructor.
   *
   * @param array $values
   * @param string $entity_type
   */
  public function __construct(array $values, $entity_type)
  {
    parent::__construct($values, $entity_type);
  }

  /**
   * Get Aperto Config Element
   *
   * @return ApertoConfigElementInterface
   */
  public function getElement(): ApertoConfigElementInterface
  {
    return ApertoConfigElement::getInstance($this);
  }

  public function getValue()
  {
    return $this->getElement()->getValue();
  }

  /**
   * Get a Form API array for config element
   *
   * @return array
   */
  public function getValueFormElement(): array
  {
    $result = $this->getElement()->getFormElement();

    return $result;
  }

  /**
   * Get parent select element
   *
   * @return array
   */
  public function getParentSelectElement(): array
  {
    $result = [
      '#type' => 'select',
      '#options' => $this->getParentSelectOptions(),
      '#default_value' => $this->parent,
      '#empty_value' => '',
      '#required' => FALSE,
    ];

    return $result;
  }

  /**
   * Get parent config entity
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function getParent()
  {
    if (!$this->parent) {
      return NULL;
    }

    return \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->load($this->parent);
  }

  /**
   * Get options for parent select element
   *
   * @return array
   */
  public function getParentSelectOptions(): array
  {
    $result = [];
    $configs = \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->loadMultiple();

    foreach ($configs as $id => $config) {
      if ($config->getElement()->isContainer() && ($id !== $this->id) && ($config->parent !== $this->id)) {
        $result[$id] = $config->get('label');
      }
    }

    return $result;
  }

  /**
   * Get all children of current config entity
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getChildren()
  {
    return \Drupal::entityTypeManager()->getStorage('aperto_config_entity')->loadByProperties(['parent' => $this->id]);
  }

  /**
   * Delete the config entity and move all config entity children to config entity parent or root
   */
  public function delete()
  {
    if (!$this->isNew() && $this->getElement()->isContainer()) {
      foreach ($this->getChildren() as $child) {
        $child->parent = $this->parent;
        $child->weight = $this->weight;
        $child->save();
      }
    }

    parent::delete();
  }
}