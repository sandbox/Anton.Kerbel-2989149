<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 26.06.18
 * Time: 15:35
 */

namespace Drupal\aperto_config_entity;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Class ApertoConfigEntityDragabbleListBuilder
 *
 * @package Drupal\aperto_config_entity
 */
class ApertoConfigEntityDragabbleListBuilder extends DraggableListBuilder
{
  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aperto_config_entity_dragabble_list';
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'aperto_config_entity';
  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildHeader() {
    $header['label'] = $this->t('Variable');
    $header['machine_name'] = $this->t('Machine Name');
    $header['type'] = $this->t('Type');
    $header['value'] = $this->t('Value');
    $header['description'] = $this->t('Description');
    $header['required'] = $this->t('Required');
    $header['weight'] = $this->t('Weight');
    $header['parent'] = $this->t('Parent');

    return $header + parent::buildHeader();
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return array
   *   A render array of the table row for displaying the entity.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildRow(EntityInterface $entity) {

    if ($entity->getElement()->isTabs()) {
      \Drupal::messenger()->addMessage(t('Elements in tabs must be wrapped by field sets'),'warning');
    }

    $row['#attributes']['class'][] = 'draggable';
    if (!$entity->getElement()->isContainer()) {
      $row['#attributes']['class'][] = 'tabledrag-leaf';
    }
    $row['#weight'] = $entity->weight;

    // Indent item on load.
    if (isset($entity->depth) && $entity->depth > 0) {
      $indentation = [
        '#theme' => 'indentation',
        '#size' => $entity->depth,
      ];
    }
    // Some table columns containing raw markup.
    $row['label'] = [
      '#markup' => $entity->label,
      '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
    ];

    $row['machine_name'] = ['#markup' => $entity->id()];
    $row['type'] = ['#markup' => t($entity->getElement()->getLabel())];
    if (strlen($entity->value) > 256) {
      $row['value'] = ['#markup' => substr($entity->value, 0, 253) . '...'];
    } else {
      $row['value'] = ['#markup' => $entity->value];
    }
    $row['description'] = ['#markup' => $entity->description];
    $row['required'] = [
      '#type' => 'checkbox',
      '#default_value' => $entity->required,
      '#disabled' => $entity->getElement()->isContainer(),
    ];

    // This is hidden from #tabledrag array (above).
    // TableDrag: Weight column element.
    $row['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for ID @id', ['@id' => $entity->id]),
      '#title_display' => 'invisible',
      '#default_value' => $entity->weight,
      // Classify the weight element for #tabledrag.
      '#attributes' => [
        'class' => ['row-weight'],
      ],
    ];
    $row['parent']['id'] = [
      '#parents' => [$this->entitiesKey, $entity->id, 'id'],
      '#type' => 'hidden',
      '#value' => $entity->id,
      '#attributes' => [
        'class' => ['row-id'],
      ],
    ];
    $row['parent']['pid'] = [
      '#parents' => [$this->entitiesKey, $entity->id, 'pid'],
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Parent ID'),
      '#default_value' => $entity->parent,
      '#attributes' => [
        'class' => ['row-pid'],
      ],
    ];

    $row['operations']['data'] = $this->buildOperations($entity);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',

      '#header' => $this->buildHeader(),
      '#empty' => $this->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'row-pid',
          'source' => 'row-id',
          'hidden' => TRUE, /* hides the WEIGHT & PARENT tree columns below */
          'limit' => FALSE,
        ],
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'row-weight',
        ],
      ],
    ];

    $this->entities = $this->load();
    $results = self::getData();

    foreach ($results as $row) {
      $form[$this->entitiesKey][$row->id] = $this->buildRow($row);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Get a list of configs by their container
   *
   * @param string $parent
   * @return array
   */
  private function getDataByParent(string $parent = ''): array
  {
    $configs = $this->load();
    $result = array_filter($configs, function($item) use ($parent){ return ($item->parent == $parent); });
    return $result;
  }

  /**
   * Retrieves the tree structure from database, sorts by parent/child/weight.
   *
   * The sorting should result in children items immediately following their
   * parent items, with items at the same level of the hierarchy sorted by
   * weight.
   *
   * The approach used here may be considered too database-intensive.
   * Optimization of the approach is left as an exercise for the reader. :)
   *
   * @return array
   *   An associative array storing our ordered tree structure.
   */
  private function getData() {

    $root_items = $this->getDataByParent();

    // Initialize a variable to store our ordered tree structure.
    $tree = [];

    // Depth will be incremented in our getTree()
    // function for the first parent item, so we start it at -1.
    $depth = -1;

    // Loop through the root item, and add their trees to the array.
    foreach ($root_items as $root_item) {
      $this->getTree($root_item, $tree, $depth);
    }

    return $tree;
  }

  /**
   * Recursively adds $item to $item_tree, ordered by parent/child/weight.
   *
   * @param mixed $item
   *   The item.
   * @param array $tree
   *   The item tree.
   * @param int $depth
   *   The depth of the item.
   */
  private function getTree($item, array &$tree = [], &$depth = 0) {
    // Increase our $depth value by one.
    $depth++;

    // Set the current tree 'depth' for this item, used to calculate
    // indentation.
    $item->depth = $depth;

    // Add the item to the tree.
    $tree[$item->id] = $item;

    $children = $this->getDataByParent($item->id);

    foreach ($children as $child) {
      // Make sure this child does not already exist in the tree, to
      // avoid loops.
      if (!in_array($child->id, array_keys($tree))) {
        // Add this child's tree to the $itemtree array.
        $this->getTree($child, $tree, $depth);
      }
    }

    // Finished processing this tree branch.  Decrease our $depth value by one
    // to represent moving to the next branch.
    $depth--;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->entities[$id]->get('required') != $value['required']) {
        $this->entities[$id]->set('required', $value['required']);
      }
      if (isset($this->entities[$id]) && $this->entities[$id]->get('parent') != $value['pid']) {
        $this->entities[$id]->set('parent', $value['pid']);
      }

      $this->entities[$id]->save();
    }

    $this->messenger()->addMessage($this->t('The variables have been updated.'));
  }

}