<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 24.07.18
 * Time: 12:24
 */

namespace Drupal\aperto_config_entity\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class ApertoConfigEntityListTask extends DeriverBase
{
  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $this->derivatives['aperto_config_entity.list'] = [
      'title' => t('Aperto Config'),
      'route_name' => 'aperto_config_entity.list',
      'base_route' => 'aperto_config_entity.list',
    ];

    $languageManager = \Drupal::languageManager();
    $languages = $languageManager->getLanguages();
    $defaultLangcode = $languageManager->getDefaultLanguage()->getId();

    foreach ($languages as $langcode => $language) {
      if ($langcode != $defaultLangcode) {
        $this->derivatives['aperto_config_entity.list.' . $langcode] = [
          'id' => 'aperto_config_entity.list.' . $langcode,
          'title' => t($language->getName()),
          'route_name' => 'aperto_config_entity.list.language',
          'parent_id' => 'aperto_config_entity:aperto_config_entity.list',
          'route_parameters' => [
            'langcode' => $langcode,
          ],
        ];
      } else {
        $this->derivatives['aperto_config_entity.list.' . $langcode] = [
          'id' => 'aperto_config_entity.list.' . $langcode,
          'title' => t($language->getName()),
          'route_name' => 'aperto_config_entity.list',
          'parent_id' => 'aperto_config_entity:aperto_config_entity.list',
        ];
      }
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }
}