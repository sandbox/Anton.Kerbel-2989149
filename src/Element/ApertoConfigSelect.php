<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:52
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;
use \Drupal\aperto_config_entity\Element\Traits\ApertoConfigElementHasOptionsTrait;

class ApertoConfigSelect extends ApertoConfigElement
{
  use ApertoConfigElementHasOptionsTrait;

  public function getFormElement(): array
  {
    $element = [
      '#type' => self::getType(),
      '#default_value' => $this->apertoConfigEntity->value ?? NULL,
      '#required' => $this->apertoConfigEntity->required,
      '#options' => $this->getOptions(),
    ];
    if (!$this->apertoConfigEntity->required) {
      $element['#empty_value'] = '';
    }

    return $element;
  }

  public static function getType(): string
  {
    return 'select';
  }

  public static function getLabel(): string
  {
    return 'Select';
  }

  public function settingsForm(): array
  {
    $form['options'] = $this->buildOptions();
    return $form;
  }

  /**
   * Prepare settings array from add/edit form values for saving
   *
   * @param array $values
   * @return array
   */
  public function prepareSettings(array $values): array
  {
    return [
      'options' => $this->prepareOptions($values),
    ];
  }

}