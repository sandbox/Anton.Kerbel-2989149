<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:52
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;

class ApertoConfigTextformat extends ApertoConfigElement
{

  public function getFormElement(): array
  {
    $element = [
      '#type' => 'text_format',
      '#default_value' => $this->apertoConfigEntity->getValue()['value'],
      '#required' => $this->apertoConfigEntity->required,
    ];

    if ($this->apertoConfigEntity->settings['value_format']) {
      $element['#format'] = $this->apertoConfigEntity->settings['value_format'];
    }
    elseif ($this->apertoConfigEntity->settings['format']) {
      $element['#format'] = $this->apertoConfigEntity->settings['format'];
    }

    $allowedFormats = $this->getAllowedFormats();
    if (!empty($allowedFormats)) {
      $element['#allowed_formats'] = $this->getAllowedFormats();
    }

    return $element;
  }

  public function setValue($value)
  {
    $this->apertoConfigEntity->value = $value['value'];
    $this->apertoConfigEntity->settings['value_format'] = $value['format'];
  }

  public function getValue()
  {
    return [
      'value' => $this->apertoConfigEntity->value,
      'format' => $this->apertoConfigEntity->settings['value_format'],
    ];
  }

  public static function getType(): string
  {
    return 'textformat';
  }

  public static function getLabel(): string
  {
    return 'Text Format';
  }

  public function settingsForm(): array
  {
    $formats = $this->getTextFormats();
    return [
      'format' => [
        '#type' => 'select',
        '#title' => t('The text format ID to preselect'),
        '#description' => t('If omitted, the default format for the current user will be used.'),
        '#empty_value' => '',
        '#required' => FALSE,
        '#options' => $formats,
        '#default_value' => $this->apertoConfigEntity->settings['format'],
      ],
      'allowed_formats' => [
        '#type' => 'checkboxes',
        '#title' => t('Text format IDs that are available for this element'),
        '#description' => t('If omitted, all text formats that the current user has access to will be allowed.'),
        '#options' => $formats,
        '#default_value' => $this->getAllowedFormats(),
      ],
    ];
  }

  private function getTextFormats()
  {
    $result = [];
    $formats = \Drupal::entityTypeManager()->getStorage('filter_format')->loadMultiple();

    foreach ($formats as $format) {
      $result[$format->get('format')] = t($format->get('name'));
    }
    return $result;
  }

  private function getAllowedFormats()
  {
    $result = [];
    foreach ($this->apertoConfigEntity->settings['allowed_formats'] as $key => $format) {
      if ($format) {
        $result[] = $key;
      }
    }
    return $result;
  }
}