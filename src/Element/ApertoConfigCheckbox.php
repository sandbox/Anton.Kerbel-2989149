<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:52
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;

class ApertoConfigCheckbox extends ApertoConfigElement
{

  public function getFormElement(): array
  {
    $element = [
      '#type' => self::getType(),
      '#default_value' => $this->apertoConfigEntity->value,
      '#required' => $this->apertoConfigEntity->required,
    ];

    return $element;
  }

  public static function getType(): string
  {
    return 'checkbox';
  }

  public static function getLabel(): string
  {
    return 'Checkbox';
  }

  public function settingsForm(): array
  {
    return ['#markup' => 'No additional settings'];
  }
}