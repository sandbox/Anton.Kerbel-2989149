<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 04.07.18
 * Time: 15:51
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;

class ApertoConfigEntityautocomplete extends ApertoConfigElement
{
  public function getFormElement(): array
  {
    $element = [
      '#type' => 'entity_autocomplete',
      '#target_type' => $this->apertoConfigEntity->settings['entity_type'],
      '#default_value' => $this->getEntity($this->apertoConfigEntity->value),
      '#required' => $this->apertoConfigEntity->required,
    ];

    return $element;
  }

  private function getEntity($value)
  {
    try {
      return \Drupal::entityTypeManager()->getStorage($this->apertoConfigEntity->settings['entity_type'])->load($value);
    } catch (\Exception $e) {
      \Drupal::messenger()->addMessage(t($e->getMEssage()), 'error');
    }

    return null;
  }

  public static function getType(): string
  {
    return 'entityautocomplete';
  }

  public static function getLabel(): string
  {
    return 'Entity Autocomplete';
  }

  public function setValue($value)
  {
    $this->apertoConfigEntity->value = $value ?? '';
  }

  /**
   * Entity autocomplete field cannot be null
   */
  public function setEmpty()
  {
    $this->apertoConfigEntity->value = '';
  }

  public function settingsForm(): array
  {
    return [
      'entity_type' => [
        '#type' => 'select',
        '#title' => 'Entity Type',
        '#options' => $this->getEntityTypeOptions(),
        '#empty_value' => '',
        '#default_value' => $this->apertoConfigEntity->settings['entity_type'],
        '#required' => TRUE,
      ],
    ];
  }

  /**
   * Get options for EntityType field
   *
   * @return array
   */
  private function getEntityTypeOptions(): array
  {
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();

    return array_combine(
      array_keys($entity_types),
      array_map(
        function($element) { return (string)$element->getLabel(); },
        $entity_types
      )
    );
  }
}