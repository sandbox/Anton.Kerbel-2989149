<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:52
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;
use \Drupal\aperto_config_entity\Element\Traits\ApertoConfigElementContainerTrait;


class ApertoConfigTabs extends ApertoConfigElement
{
  use ApertoConfigElementContainerTrait;

  public function getFormElement(): array
  {
    $element = [
      '#type' => 'vertical_tabs',
      '#default_tab' => $this->apertoConfigEntity->settings['default_tab'],
    ];

    return $element;
  }

  public static function getType(): string
  {
    return 'tabs';
  }

  public static function getLabel(): string
  {
    return 'Vertical Tabs';
  }

  public function settingsForm(): array
  {
    $options = $this->getTabOptions();
    if (!empty($options)) {
      return [
        'default_tab' => [
          '#type' => 'radios',
          '#title' => 'Default tab',
          '#options' => $options,
          '#default_value' => $this->apertoConfigEntity->settings['default_tab'],
        ],
      ];
    }

    return ['#markup' => t('No additional settings')];
  }

  private function getTabOptions(): array
  {
    $result = [];

    $children = $this->apertoConfigEntity->getChildren();
    foreach ($children as $child) {
      if ($child->getElement()->isContainer() && !$child->getElement()->isTabs()) {
        $result[$child->id] = $child->label;
      }
    }
    return $result;
  }

}