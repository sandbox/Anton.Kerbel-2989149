<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 25.06.18
 * Time: 16:52
 */

namespace Drupal\aperto_config_entity\Element;

use \Drupal\aperto_config_entity\ApertoConfigElement;
use \Drupal\aperto_config_entity\Element\Traits\ApertoConfigElementContainerTrait;


class ApertoConfigDetails extends ApertoConfigElement
{
  use ApertoConfigElementContainerTrait;

  public function getFormElement(): array
  {
    $element = [
      '#type' => self::getType(),
      '#open' => $this->apertoConfigEntity->settings['open'],
      '#attributes' => [
        'id' => $this->apertoConfigEntity->id,
      ],
    ];

    $parent = $this->apertoConfigEntity->getParent();
    if ($parent && $parent->getElement()->isTabs()) {
      $element['#group'] = $parent->id;
    }

    return $element;
  }

  public static function getType(): string
  {
    return 'details';
  }

  public static function getLabel(): string
  {
    return 'Field Set';
  }

  public function settingsForm(): array
  {
    return [
      'open' => [
        '#type' => 'checkbox',
        '#title' => t('Open by default'),
        '#default_value' => $this->apertoConfigEntity->settings['open'],
      ],
    ];
  }

}