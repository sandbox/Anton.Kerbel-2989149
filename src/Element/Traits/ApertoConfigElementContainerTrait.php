<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 04.07.18
 * Time: 10:52
 */

namespace Drupal\aperto_config_entity\Element\Traits;


trait ApertoConfigElementContainerTrait
{

  protected $isContainer = TRUE;

}