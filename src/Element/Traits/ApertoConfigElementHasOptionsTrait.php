<?php
/**
 * Created by PhpStorm.
 * User: anton.kerbel
 * Date: 13.07.18
 * Time: 15:22
 */

namespace Drupal\aperto_config_entity\Element\Traits;


trait ApertoConfigElementHasOptionsTrait
{

  /**
   * Get Form API elements to show dragabble table of options
   *
   * @return array
   */
  public function buildOptions(): array
  {
    $options = [
      '#type' => 'table',
      '#header' => [
        t('Key'),
        t('Option'),
        t('Weight'),
      ],
      '#empty' => t('Sorry, There are no items!'),
      // TableDrag: Each array value is a list of callback arguments for
      // drupal_add_tabledrag(). The #id of the table is automatically
      // prepended; if there is none, an HTML ID is auto-generated.
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    if (!empty($this->apertoConfigEntity->settings['options'])) {
      foreach ($this->apertoConfigEntity->settings['options'] as $option) {
        // TableDrag: Mark the table row as draggable.
        $options[] = $this->buildOption($option);
      }
    }

    $options[] = $this->buildOption(['key' => '', 'option' => '', 'weight' => 0]);

    return $options;
  }

  /**
   * Get Form API element to show an option
   *
   * @param array $option ['key' => 'option_key', 'option' => 'option_label', 'weight' => 100500]
   * @return array
   */
  public function buildOption(array $option = []): array
  {
    // TableDrag: Mark the table row as draggable.
    $optionElement['#attributes']['class'][] = 'draggable';
    // TableDrag: Sort the table row according to its existing/configured
    // weight.
    $optionElement['#weight'] = $option['weight'];

    // Some table columns containing raw markup.
    $optionElement['key'] = [
      '#type' => 'textfield',
      '#default_value' => $option['key'],
    ];
    $optionElement['option'] = [
      '#type' => 'textfield',
      '#default_value' => $option['option'],
    ];
    // TableDrag: Weight column element.
    $optionElement['weight'] = [
      '#type' => 'weight',
      '#title' => t('Weight for @title', ['@title' => t('Option')]),
      '#title_display' => 'invisible',
      '#default_value' => $option['weight'],
      // Classify the weight element for #tabledrag.
      '#attributes' => ['class' => ['table-sort-weight']],
    ];

    return $optionElement;
  }

  /**
   * Get options for Form API elements such as "select" or "radios"
   *
   * @return array
   */
  public function getOptions(): array
  {
    $result = [];

    foreach ($this->apertoConfigEntity->settings['options'] as $option) {
      $result[$option['key']] = t($option['option']);
    }

    return $result;
  }

  /**
   * Prepare options data for saving in DB. It cleans empty option elements.
   *
   * @param array $values
   * @return array
   */
  public function prepareOptions(array $values): array
  {
    $result = [];

    foreach ($values['options'] as $option)
    {
      if (!empty($option['key']) && !empty($option['option'])) {
        $result[] = $option;
      }
    }
    return $result;
  }
}